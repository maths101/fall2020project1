//Daniel Lam ID: 1932789
package movies.importer;

import java.util.ArrayList;

public class KaggleProcessor extends Processor
{
	//Constructor
	public KaggleProcessor(String inputDir, String destinationDir) 
	{
		super(inputDir, destinationDir, true);
	}
	
	/**
	 * Processes through the input ArrayList<String> and extracts the title, releaseYear, runTime and source of each movie and adds it to another text file
	 * @author Daniel
	 * @param Takes as input an ArrayList<String> of Strings separated by a line break. (In this case, the contents of Kaggle Movie List)
	 * @return Returns an ArrayList<String> of the title, releaseYear, runTime and source of each movie of the input
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input)
	{
		ArrayList<String> finaltxtcontents = new ArrayList<String>();
		int nbOfColumns = 21;
		int columnMovieName = 15;
		int columnYear = 20;
		int columnRunTime = 13;
		for (String line : input)
		{
			String[] lineArr = line.split("\t");
			if (lineArr.length == nbOfColumns)
			{
				Movie movieObj = new Movie(lineArr[columnMovieName], lineArr[columnYear], lineArr[columnRunTime], "Kaggle");
				finaltxtcontents.add(movieObj.toString());
			}
		}
		return finaltxtcontents;
	}
}
