//Daniel Lam ID: 1932789
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;

/**
 * JUnit tests for Normalizer.java
 * @author Daniel
 *
 */
class NormalizerTests 
{
	/**
	 * Test constructor of Normalizer.java
	 * @author Daniel
	 * 
	 */
	@Test
	public void testConstructor() 
	{
		try 
		{
			Normalizer test = new Normalizer("Files\\NormalizerInput","Files\\ValidatorInput");
		}
		catch (Exception e)
		{
			fail("Constructor failed!");
		}
	}
	
	/**
	 * Tests the process() method from Normalizer
	 * @author Daniel
	 * 
	 */
	@Test
	public void testProcess()
	{
		ArrayList<String> test = new ArrayList<String>();
		test.add("The Mummy: Tomb of the Dragon Emperor	2008	112 minutes	Kaggle");
		test.add("The Masked Saint	2016	111 minutes	Kaggle");
		
		ArrayList<String> expectedResult = new ArrayList<String>();
		expectedResult.add("the mummy: tomb of the dragon emperor	2008	112	Kaggle");
		expectedResult.add("the masked saint	2016	111	Kaggle");
		
		Normalizer testProcessor = new Normalizer("", "");
		ArrayList<String> testResults = testProcessor.process(test);
		
		assertEquals(testResults.get(0), expectedResult.get(0));
		assertEquals(testResults.get(1), expectedResult.get(1));
	}
}
