//Grigor Mihaylov (1937997)
package movies.tests;

import movies.importer.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the imdbImporter class
 * @author Grigor (R2D2)
 * 
 */
class ImdbImporterTest {
	
	/**
	 * Tests the process method
	 * @author Grigor (R2D2)
	 * 
	 */
	@Test
	public void processTest() {
		ArrayList<String> test = new ArrayList<String>();
		test.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		test.add("tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"				7	7");
		
		ArrayList<String> expectedResult = new ArrayList<String>();
		expectedResult.add("Miss Jerry	1894	45	imdb");
		expectedResult.add("The Story of the Kelly Gang	1906	70	imdb");
		
		ImdbImporter importer = new ImdbImporter("","");
		ArrayList<String> trueResult = importer.process(test);
		
		assertEquals(trueResult.get(0),  expectedResult.get(0));
		assertEquals(trueResult.get(1),  expectedResult.get(1));
	}
	
}
