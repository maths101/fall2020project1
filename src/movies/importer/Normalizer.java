//Daniel Lam ID: 1932789
package movies.importer;

import java.util.ArrayList;

public class Normalizer extends Processor
{
	//Constructor
	public Normalizer(String inputDir, String destinationDir) 
	{
		super(inputDir, destinationDir, false);
	}
	
	/**
	 * process() normalizes the KaggleProcessor.java by transforming title into lower case and giving only minutes of runTime
	 * @author Daniel
	 * @param Takes in param an ArrayList<String> with Strings separated with a line break from a text file
	 * @return Returns ArrayList<String> of normalized movie contents (lowercase title, releaseYear, only minutes of runTime and source)
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input)
	{
		ArrayList<String> normalizedTextContents = new ArrayList<String>();
		for (String movieContents : input)
		{
			String[] movieContentsArr = movieContents.split("\t");
			Movie movieObj = new Movie(movieContentsArr[0].toLowerCase(), movieContentsArr[1], normalizeRunTime(movieContentsArr[2]), movieContentsArr[3]);
			normalizedTextContents.add(movieObj.toString());
		}
		return normalizedTextContents;
	}
	/**
	 * normalizeRunTime() takes the full string of runTime and only returns the minutes of runTime
	 * @author Daniel
	 * @param Full String of runTime (e.g. "133 minutes")
	 * @return Only minutes of the runTime String (e.g. "133")
	 * 
	 */
	private String normalizeRunTime(String runTime)
	{
		int indexSpace = runTime.indexOf(' ');
		if(indexSpace != -1) {
			return runTime.substring(0,indexSpace);
		}
		return runTime;
	}
}
