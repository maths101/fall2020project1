//Daniel Lam ID: 1932789
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleProcessor;

/**
 * JUnit test cases for KaggleProcessor.java
 * @author Daniel
 *
 */
class KaggleProcessorTests 
{
	/**
	 * Tests constructor of KaggleProcessor.java
	 * @author Daniel
	 * 
	 */
	@Test
	public void testConstructor() 
	{
		try 
		{
			KaggleProcessor test = new KaggleProcessor("Files\\KaggleRawFiles", "Files\\NormalizerInput");
		}
		catch (Exception e)
		{
			fail("Constructor failed!");
		}
	}
	/**
	 * Tests the process() method from KaggleProcessor
	 * @author Daniel
	 * 
	 */
	@Test
	public void testProcess()
	{
		ArrayList<String> test = new ArrayList<String>();
		test.add("Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		test.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\"	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");
		
		ArrayList<String> expectedResult = new ArrayList<String>();
		expectedResult.add("The Mummy: Tomb of the Dragon Emperor	2008	112 minutes	Kaggle");
		expectedResult.add("The Masked Saint	2016	111 minutes	Kaggle");
		
		KaggleProcessor testProcessor = new KaggleProcessor("", "");
		ArrayList<String> testResults = testProcessor.process(test);
		
		assertEquals(testResults.get(0), expectedResult.get(0));
		assertEquals(testResults.get(1), expectedResult.get(1));
	}
}
