//Grigor Mihaylov (1937997)
package movies.importer;

import java.util.ArrayList;

/**
 * imdbImporter class takes a text file from the imdbRawFiles and makes a text file with a String formatted based on the Movie object
 * @author Grigor (R2D2)
 * 
 */
public class ImdbImporter extends Processor {
	
	//Constructor
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	/** 
	 * this process is taking the specific part in each line, making a Movie object and then creating a new line based on that object
	 * @param input An ArrayList<String> representing the input to the processor
	 * @return Returns an ArrayList<String> representing the results of applying this processor
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> movies = new ArrayList<String>();
		for (String s: input) {
			String[] contents = s.split("\\t",-1);
			Movie movieObj = new Movie(contents[1], contents[3], contents[6], "imdb");
			movies.add(movieObj.toString());
		}
		return movies;
	}
}	
