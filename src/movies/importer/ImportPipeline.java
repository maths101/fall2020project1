//Daniel Lam (1932789), Grigor Mihaylov (1937997)
package movies.importer;

import java.io.IOException;

/**
 * Main method and Runs the pipeline
 * @author Grigor Mihaylov (R2D2)
 * @author Daniel Lam (C3PO) 
 * 
 */
public class ImportPipeline {
	/**
	 * Creating all of the Processors in a Processor Array and calling processAll() on it
	 * @author Grigor Mihaylov (R2D2)
	 * @author Daniel Lam (C3PO) 
	 * 
	 */
	public static void main(String[] args) throws IOException {
		Processor[] pipeline= new Processor[5];
		pipeline[0] = new ImdbImporter("Files\\ImdbRawFiles", "Files\\NormalizerInput");
		pipeline[1] = new KaggleProcessor("Files\\KaggleRawFiles", "Files\\NormalizerInput");
		pipeline[2] = new Normalizer("Files\\NormalizerInput", "Files\\ValidatorInput");
		pipeline[3] = new Validator("Files\\ValidatorInput", "Files\\DeduperInput");
		pipeline[4]	= new Deduper("Files\\DeduperInput", "Files\\Final");
		processAll(pipeline);
	}
	/**
	 * For each Processor object in the pipeline, invoke its execute method 
	 * @param the Processor Array that represents the pipeline
	 * @author Grigor Mihaylov (R2D2)
	 * @author Daniel Lam (C3PO) 
	 * 
	 */
	public static void processAll(Processor[] pipeline) throws IOException {
		for (Processor processorObj : pipeline) {
			processorObj.execute();
		}
	}
}
