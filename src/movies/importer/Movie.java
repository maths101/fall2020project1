//Daniel Lam (1932789), Grigor Mihaylov (1937997)
package movies.importer;

/**
 * Defines an Object that stores information about a movie
 * @author Grigor Mihaylov (R2D2)
 * @author Daniel Lam (C3PO) 
 * 
 */
public class Movie {
	
	//Variables
	private String releaseYear;
	private String movieName;
	private String runTime;
	private String source;
	
	//Constructor
	public Movie(String movieName, String releaseYear, String runTime, String source) {
		this.movieName = movieName;
		this.releaseYear = releaseYear;
		this.runTime = runTime;
		this.source = source;
	}
	
	/**
	 * Get method for the Release Year
	 * @return the Release Year
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	public String getReleaseYear() {
		return this.releaseYear;
	}
	
	/**
	 * Get method for the Movie Name
	 * @return the Name of the Movie
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	public String getMovieName() {
		return this.movieName;
	}
	
	/**
	 * Get method for Run Time
	 * @return the Run Time
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	public String getRunTime() {
		return this.runTime;
	}
	
	/**
	 * Get method for the Movie's Source
	 * @return the Source 
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	public String getSource() {
		return this.source;
	}
	
	/**
	 * Overridden toString method that return a String representing the movie
	 * @return a String representing the movie
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	@Override
	public String toString() {
		return this.movieName + "	" + this.releaseYear + "	" + this.runTime + "	" + this.source;
	}
	/**
	 * Overridden equals method that return a boolean based on if the movies are the same
	 * @return boolean based on if the movies are the same
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Movie)) {
			return false;
		} else {
			Movie other = (Movie)obj;
			int thisRunTime = Integer.parseInt(this.runTime);
			int otherRunTime = Integer.parseInt(other.runTime);
			if(this.movieName.equals(other.movieName) && this.releaseYear.equals(other.releaseYear) && Math.abs(thisRunTime - otherRunTime) <= 5) {
				return true;
			}
		}
		return false;
	}
}
