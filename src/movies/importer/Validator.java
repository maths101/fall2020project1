//Grigor Mihaylov (1937997)
package movies.importer;

import java.util.ArrayList;
/**
 * Validator makes sure that all the data is proper and no data is missing
 * @author Grigor (R2D2)
 * 
 */
public class Validator extends Processor {
	
	//Constructor
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	/**
	 * The process method makes sure that all the data is proper and no data is missing
	 * @author Grigor (R2D2)
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<Integer> indexesToRemove= new ArrayList<Integer>();
		
		for (int i = 0; i < input.size(); i++) {
			String[] contents = input.get(i).split("\\t",-1);
			try {
				Movie movieObj = new Movie(contents[0],contents[1],contents[2],contents[3]);
				int year =  Integer.parseInt(movieObj.getReleaseYear());
				int runTime = Integer.parseInt(movieObj.getRunTime());
				
				for(String s : contents) {
					if(s == "") {
						indexesToRemove.add(i);
					}
				}
			}
			catch(Exception e) {
				indexesToRemove.add(i);
			}
		}		
		ArrayList<String> validatedMovies = input;
		for (Integer i : indexesToRemove) {
			int realInt = i;
			validatedMovies.remove(realInt);
		}
		return validatedMovies;
	}
}
