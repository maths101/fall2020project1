//Grigor Mihaylov (1937997)
package movies.tests;

import movies.importer.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the Validator class
 * @author Grigor (R2D2)
 * 
 */
class ValidatorTest {
	/**
	 * Testing the process method
	 * @author Grigor (R2D2)
	 * 
	 */
	@Test
	public void processTest() {
		Validator validatorObj = new Validator("", "");
		
		ArrayList<String> case1 = new ArrayList<String>();
		case1.add("Miss Jerry	1894	45 minutes	imdb");
		ArrayList<String> result1 = validatorObj.process(case1);
		assertEquals(result1.size(), 0);
		
		ArrayList<String> case2 = new ArrayList<String>();
		case2.add("Miss Jerry	year was 1894	45	imdb");
		ArrayList<String> result2 = validatorObj.process(case2);
		assertEquals(result2.size(), 0);
		
		ArrayList<String> case3 = new ArrayList<String>();
		case3.add("Miss Jerry		45	imdb");
		ArrayList<String> result3 = validatorObj.process(case3);
		assertEquals(result3.size(), 0);
		
		ArrayList<String> case4 = new ArrayList<String>();
		case4.add("Miss Jerry	1894");
		ArrayList<String> result4 = validatorObj.process(case4);
		assertEquals(result4.size(), 0);	
		
		ArrayList<String> case5 = new ArrayList<String>();
		case5.add("Miss Jerry	1894	45	imdb");
		ArrayList<String> result5 = validatorObj.process(case5);
		assertEquals(result5.size(), 1);	
	}
}
