//Daniel Lam (1932789) and Grigor Mihaylov (1937997)
package movies.tests;

import movies.importer.Movie;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the Movie class
 * @author Grigor (R2D2)
 * @author Daniel (C3PO)
 * 
 */
class MovieTest {
	/**
	 * Test the constructor and the toString method of the Movie class
	 * @author Grigor
	 * @author Daniel
	 * 
	 */
	@Test
	public void testConstructAndToString() {
		Movie testMovie1 = new Movie("Guardians of the Galaxy", "2014", "121 minutes", "Kaggle");
		assertEquals("Guardians of the Galaxy" + "	" +"2014" + "	" + "121 minutes" + "	" +	"Kaggle", testMovie1.toString());
	}
	
	/**
	 * Test the get methods of the Movie class
	 * @author Grigor
	 * @author Daniel
	 */
	@Test
	public void testGetMethods() {
		Movie testMovie2 = new Movie("Pulp Fiction", "1994", "154 minutes", "Imdb");
		assertEquals("Pulp Fiction", testMovie2.getMovieName());
		assertEquals("1994", testMovie2.getReleaseYear());
		assertEquals("154 minutes", testMovie2.getRunTime());
		assertEquals("Imdb", testMovie2.getSource());
	}
	/**
	 * Test the equals methods of the Movie class
	 * @author Grigor
	 * @author Daniel
	 */
	@Test
	public void testEqualsMethods() {
		Movie testMovie1 = new Movie("Pulp Fiction", "1994", "154", "Imdb");
		Movie testMovie2 = new Movie("Pulp Fiction", "1994", "150", "Kaggle");
		Movie testMovie3 = new Movie("Pulp Fi", "1994", "150", "Kaggle");
		Movie testMovie4 = new Movie("Pulp Fiction", "1994", "6", "Kaggle");
		
		assertEquals(testMovie1.equals(testMovie2), true);
		assertEquals(testMovie1.equals(testMovie3), false);
		assertEquals(testMovie1.equals(testMovie4), false);
	
	}
}
