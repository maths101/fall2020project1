//Daniel Lam (1932789), Grigor Mihaylov (1937997)
package movies.importer;

import java.util.ArrayList;

/**
 * Makes sure that there are no duplicate movies in that final file
 * @author Grigor Mihaylov (R2D2)
 * @author Daniel Lam (C3PO) 
 * 
 */
public class Deduper extends Processor {
	
	//Constructor
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	/**
	 * In case of duplicates, the process method merges the two records into one
	 * @author Grigor Mihaylov (R2D2)
	 * @author Daniel Lam (C3PO) 
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<Movie> movies = new ArrayList<Movie>();
		for (String s : input) {
			String[] contents = s.split("\\t", -1);
			Movie newMovie = new Movie(contents[0], contents[1], contents[2], contents[3]);
			if(!movies.contains(newMovie)) {
				movies.add(newMovie);
			} else {
				int index = movies.indexOf(newMovie);
				String oldSource = movies.get(index).getSource();
				String newSource = newMovie.getSource();
				String mergedSource = newSource;
				
				if(!oldSource.equals(newSource)) {
					mergedSource = oldSource + ";" + newSource;
				}
				
				Movie mergedMovie = new Movie(newMovie.getMovieName(), newMovie.getReleaseYear(), newMovie.getRunTime(), mergedSource);
				movies.set(index, mergedMovie);
			}
		}
		ArrayList<String> nonDupedMovies = new ArrayList<String>();
		for (Movie movie : movies) {
			 nonDupedMovies.add(movie.toString());
		}
		return nonDupedMovies;
	}

}
