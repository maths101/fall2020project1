//Daniel Lam (1932789), Grigor Mihaylov (1937997)
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;

/**
 * JUnit tests for the Deduper class
 * @author Grigor (R2D2)
 * @author Daniel (C3PO)
 * 
 */
class DeduperTest {
	/**
	 * Test for the process method
	 * @author Grigor (R2D2)
	 * @author Daniel (C3PO)
	 * 
	 */
	@Test
	public void testProcess() {
		Deduper deduperObj = new Deduper("","");
		
		ArrayList<String> expectedResult1 = new ArrayList<String>();
		expectedResult1.add("Miss Jerry	1894	45	imdb");
		expectedResult1.add("Miss Jerry	1894	46	imdb");
		ArrayList<String> realResult1 = deduperObj.process(expectedResult1);
		
		assertEquals(realResult1.get(0), "Miss Jerry	1894	46	imdb" );
		
		ArrayList<String> expectedResult2 = new ArrayList<String>();
		expectedResult2.add("Miss	1894	45	imdb");
		expectedResult2.add("Miss	1894	45	kaggle");
		ArrayList<String> realResult2 = deduperObj.process(expectedResult2);
		
		assertEquals(realResult2.get(0), "Miss	1894	45	imdb;kaggle" );	
	}
}
